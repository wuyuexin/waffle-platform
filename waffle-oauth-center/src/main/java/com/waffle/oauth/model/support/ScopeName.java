package com.waffle.oauth.model.support;

/**
 * @author yuexin
 */
public enum ScopeName {
    /**
     *
     */
    READ,

    /**
     *
     */
    WRITE,

    /**
     *
     */
    TRUST;
}
