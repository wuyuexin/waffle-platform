package com.waffle.api.blog.service.common.file;

/**
 * @author yuexin
 */
public interface FileStorageInfo {

    String getKey();

    String getPath();

    String getTarget();
}
